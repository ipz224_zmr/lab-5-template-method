﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryLibrary
{
    public class Smartphone : Device
    {
        public string Model { get; }
        public string System { get; }
        public string ScreenSize { get; }

        public Smartphone(string manufacturer, string model, string system, string screenSize) : base(manufacturer)
        {
            this.Model = model;
            this.System = system;
            this.ScreenSize = screenSize;
        }

        public override string ToString()
        {
            return base.ToString() + $"Model: {this.Model}, Operating System: {this.System}, Screen Size: {this.ScreenSize}";
        }
    }
}
