﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsClassLibrary
{
    public class SecondLevel : Handler
    {
        protected override string Report => "Переконайтеся в тому чи наявне живлення у пристрія.";

        protected override int HelpLevel => 2;

        protected override string HandableProblem => "Пристрій не працює";

        public override void Inquiry(UserInquiry inquiry)
        {
            if (inquiry.UserProblem.Equals(this.HandableProblem, StringComparison.CurrentCultureIgnoreCase))
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine($"Рівень {this.HelpLevel} підтримка: {this.Report}");
                Console.ResetColor();
                return;
            }

            base.Inquiry(inquiry);
        }
    }
}
