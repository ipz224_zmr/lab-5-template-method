﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ClassLibrarySOLID
{

    public class WarehouseItem : Product
    {
      
        public int Quantity { get; set; }
        public DateTime LastRestockDate { get; set; }

        public WarehouseItem(string name, string unit, Money pricePerUnit, int quantity, DateTime lastRestockDate)
            : base(name, unit, pricePerUnit)
        {
            Quantity = quantity;
            LastRestockDate = lastRestockDate;
        }


        public static Dictionary<string, string> GetUserInputMoney()
        {
            Dictionary<string, string> userInput = new Dictionary<string, string>();
            ValidateAndSetInput("Price (Whole Part)", "Price (Whole Part) must be a non-empty integer. Please try again.", s => int.TryParse(s, out _), userInput);
            ValidateAndSetInput("Price (Cents)", "Price (Cents) must be a non-empty integer. Please try again.", s => int.TryParse(s, out _), userInput);
            return userInput;
        }

        public static Dictionary<string, string> GetUserInput()
        {
            Dictionary<string, string> userInput = new Dictionary<string, string>();

            ValidateAndSetInput("Name", "Name cannot be empty. Please try again.", s => !string.IsNullOrEmpty(s), userInput);
            ValidateAndSetInput("Unit of Measure", "Unit of Measure cannot be empty. Please try again.", s => !string.IsNullOrEmpty(s), userInput);
            Dictionary<string, string> userInputMoney = GetUserInputMoney();
            ValidateAndSetInput("Quantity", "Quantity must be a non-empty integer. Please try again.", s => int.TryParse(s, out _), userInput);
            ValidateAndSetInput("Last Restock Date (yyyy-MM-dd)", "Invalid date format. Please use yyyy-MM-dd format.", s => DateTime.TryParseExact(s, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out _), userInput);
            userInput["Price (Whole Part)"] = userInputMoney["Price (Whole Part)"];
            userInput["Price (Cents)"] = userInputMoney["Price (Cents)"];
            return userInput;
        }

        public static void ValidateAndSetInput(string prompt, string errorMessage, Func<string, bool> validator, Dictionary<string, string> userInput)
        {
            while (true)
            {
                Console.Write($"{prompt}: ");
                string input = Console.ReadLine().Trim();
                if (validator(input))
                {
                    userInput[prompt] = input;
                    break;
                }
                else
                {
                    Console.WriteLine(errorMessage);
                }
            }
        }


        public static WarehouseItem GetWarehouseItemFromUserInput()
        {
            Dictionary<string, string> userInput = GetUserInput();
            string name = userInput["Name"];
            string unitOfMeasure = userInput["Unit of Measure"];
            int wholePart = Convert.ToInt32(userInput["Price (Whole Part)"]);
            int cents = Convert.ToInt32(userInput["Price (Cents)"]);
            int quantity = Convert.ToInt32(userInput["Quantity"]);
            DateTime lastRestockDate = DateTime.ParseExact(userInput["Last Restock Date (yyyy-MM-dd)"], "yyyy-MM-dd", null);

            var money = new Money(wholePart, cents);
            return new WarehouseItem(name, unitOfMeasure, money, quantity, lastRestockDate);
        }
    }



}
