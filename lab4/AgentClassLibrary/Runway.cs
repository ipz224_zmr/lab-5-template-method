﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentClassLibrary
{
    public class Runway
    {
        public readonly Guid Id = Guid.NewGuid();
        public Aircraft IsBusyWithAircraft;
        public string Way { get; set; }
        public Runway(string Way)
        {
            this.Way = Way;
        }

        public bool IsAvailable()
        {
            return IsBusyWithAircraft == null;
        }
    }
}
