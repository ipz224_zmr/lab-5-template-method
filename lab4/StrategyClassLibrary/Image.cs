﻿using ComposerClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyClassLibrary
{
    public class Image : LightElementNode
    {
        private readonly IImageLoadingStrategy _loadingStrategy;
        private string _src;
        public Image(IImageLoadingStrategy loadingStrategy, string src, string displayType = "inline", List<string> cssClasses = null)
        : base("img", displayType, "selfClosing", cssClasses ?? new List<string>(), new List<LightNode>())
        {
            _loadingStrategy = loadingStrategy;
            _src = src;
        }
        public override string OuterHTML
        {
            get
            {
                string cssClassStr = string.Join(" ", CSSClasses);
                string attributes = $"class=\"{cssClassStr}\" src=\"{_src}\"";

                if (ClosingType == "selfClosing")
                    return $"<{TagName} {attributes}/>";
                else
                    return $"<{TagName} {attributes}></{TagName}>";
            }
        }
        public void DisplayImage()
        {
            byte[] imageData = _loadingStrategy.LoadImage(_src);
            Console.WriteLine($"Displaying image from {_src}");
        }
    }
}
