﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryLibrary
{
    public class BoomFactory : IDeviceFactory
    {
        public string Manufacturer => "Boom";

        public Laptop CreateLaptop()
        {
            return new Laptop(Manufacturer, "RTX 3080", "Intel Core i9", "15 inch", 32);
        }

        public Netbook CreateNetbook()
        {
            return new Netbook(Manufacturer, "BoomModel", "BoomProcessor", 16, 1024);
        }

        public EBook CreateEBook()
        {
            return new EBook(Manufacturer, "8 inch");
        }

        public Smartphone CreateSmartphone()
        {
            return new Smartphone(Manufacturer, "BoomModel", "Android", "6.5 inch");
        }
    }

}
