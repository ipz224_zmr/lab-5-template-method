﻿
using ComposerClassLibrary;
using ComposerClassLibrary.TemplateMethod;

class Program
{
    static void Main(string[] args)
    {
        var textNode = new LightTextNode("Hello, world!");
        var divElement = new LightElementNode("div", "block", "closing", new List<string> { "container" }, new List<LightNode> { textNode }); 
        divElement.RunLifecycleHooks();
        Console.WriteLine("Outer HTML:");
        Console.WriteLine(divElement.OuterHTML);
        Console.WriteLine("Inner HTML:");
        Console.WriteLine(divElement.InnerHTML);
    }
}