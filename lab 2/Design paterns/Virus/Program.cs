﻿using VirusLibrary;
internal class Program
{
    static void Main(string[] args)
    {
        Virus grandparentVirus = new Virus("Grandparent Virus", 100, 5, "Type A", new List<Virus>());
        Virus parentVirus1 = new Virus("Parent Virus 1", 80, 3, "Type B", new List<Virus>());
        Virus parentVirus2 = new Virus("Parent Virus 2", 90, 4, "Type C", new List<Virus>());

        grandparentVirus.Children.Add(parentVirus1);
        grandparentVirus.Children.Add(parentVirus2);

        Virus childVirus1 = new Virus("Child Virus 1", 60, 1, "Type D", new List<Virus>());
        Virus childVirus2 = new Virus("Child Virus 2", 70, 2, "Type E", new List<Virus>());

        parentVirus1.Children.Add(childVirus1);
        parentVirus1.Children.Add(childVirus2);

        Virus clonedGrandparent = grandparentVirus.PerformCopy();

        Console.WriteLine("Original Grandparent Virus:");
        Console.WriteLine(grandparentVirus);
        Console.WriteLine("Cloned Grandparent Virus:");
        Console.WriteLine(clonedGrandparent);
        Console.ReadLine();
    }

}