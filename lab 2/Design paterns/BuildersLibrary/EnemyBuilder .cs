﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildersLibrary
{
    public class EnemyBuilder : ICharacterBuilder
    {
        protected Character _enemy = new Character();
        public EnemyBuilder()
        {
            Reset();
        }

        public void Reset()
        {
            _enemy = new Character();
        }
        public ICharacterBuilder SetHeight(string height)
        {
            _enemy.Height = height;
            return this;
        }

        public ICharacterBuilder SetBuild(string build)
        {
            _enemy.Build = build;
            return this;
        }

        public ICharacterBuilder SetHairColor(string hairColor)
        {
            _enemy.HairColor = hairColor;
            return this;
        }

        public ICharacterBuilder SetEyeColor(string eyeColor)
        {
            _enemy.EyeColor = eyeColor;
            return this;
        }

        public ICharacterBuilder SetClothing(string clothing)
        {
            _enemy.Clothing = clothing;
            return this;
        }
        public ICharacterBuilder AddToInventory(string item)
        {
            _enemy.Inventory.Add(item);
            return this;
        }

        public Character Build()
        {
            Character enemy = _enemy;
            Reset();
            return enemy;
        }
    }
}
