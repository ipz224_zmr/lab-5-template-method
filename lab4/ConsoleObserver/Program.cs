﻿using ComposerClassLibrary;

public class Program
{
    public static void Main(string[] args)
    {
        var button = new LightElementNode("button", "inline", "closing", new List<string> { "btn", "btn-primary" }, new List<LightNode> { new LightTextNode("Click me") });

        button.AddEventListener("click", () => Console.WriteLine("Button clicked!"));
        button.TriggerEvent("click");
        button.TriggerEvent("click");
        button.TriggerEvent("click");
        button.TriggerEvent("doubleclick");
        button.TriggerEvent("click");

        button.AddEventListener("doubleclick", () => Console.WriteLine("Button doubleclicked!"));
        button.TriggerEvent("doubleclick");
        button.TriggerEvent("click");
        button.TriggerEvent("move");
    }
}