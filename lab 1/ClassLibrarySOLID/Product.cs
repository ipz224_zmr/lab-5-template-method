﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrarySOLID
{

    public class Product
    {
        public string Name { get; set; }
        public string Unit { get; set; }
        public Money Price { get; set; }
        public Product(string name, string unit, Money price)
        {
            Name = name;
            Unit = unit;
            Price = price;
        }
     
        public  void ReducePrice(Money discount)
        {
            Price.ApplyDiscount(discount);
        }
    }
}
