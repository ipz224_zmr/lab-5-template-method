﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryLibrary
{
    public class NetFactory : IDeviceFactory
    {
        public string Manufacturer => "Net";

        public Laptop CreateLaptop()
        {
            return new Laptop(Manufacturer, "GeForce GTX 1650", "Intel Core i5", "15.6 inch", 8);
        }

        public Netbook CreateNetbook()
        {
            return new Netbook(Manufacturer, "NetbookModel", "NetbookProcessor", 4, 256);
        }

        public EBook CreateEBook()
        {
            return new EBook(Manufacturer, "10 inch");
        }

        public Smartphone CreateSmartphone()
        {
            return new Smartphone(Manufacturer, "NetPhone", "Android", "6.5 inch");
        }
    }
}
