﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentClassLibrary
{
    public static class PrintText
    {
        public static void PrintSuccess(string message) =>
       PrintMessageWithColor(message, ConsoleColor.Magenta);

        public static void PrintError(string message) =>
            PrintMessageWithColor(message, ConsoleColor.DarkYellow);

        public static void PrintMessageWithColor(string message, ConsoleColor textColor)
        {
            Console.ForegroundColor = textColor;
            Console.WriteLine(message);
            Console.ResetColor();
        }

    }
}
