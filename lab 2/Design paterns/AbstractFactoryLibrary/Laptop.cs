﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryLibrary
{
    public class Laptop : Device
    {
        public string VideoCard { get; }
        public string Processor { get; }
        public string ScreenSize { get; }
        public int RAM { get; }

        public Laptop(string Manufacturer, string videoCard, string processor, string screenSize, int ram) : base(Manufacturer)
        {
            this.VideoCard = videoCard;
            this.Processor = processor;
            this.ScreenSize = screenSize;
            this.RAM = ram;
        }

        public override string ToString()
        {
            return base.ToString() + $"Video Card: {this.VideoCard}, Processor: {this.Processor}, Screen Size: {this.ScreenSize}, RAM: {this.RAM}";
        }
    }
}
