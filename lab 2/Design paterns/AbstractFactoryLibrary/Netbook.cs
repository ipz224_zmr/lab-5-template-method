﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryLibrary
{
    public class Netbook : Device
    {
        public string Model { get; }
        public string Processor { get; }
        public int RAM { get; }
        public int HardDriveSize { get; }

        public Netbook(string manufacturer, string model, string processor, int ram, int hardDriveSize) : base(manufacturer)
        {
            this.Model = model;
            this.Processor = processor;
            this.RAM = ram;
            this.HardDriveSize = hardDriveSize;
        }

        public override string ToString()
        {
            return base.ToString() + $"Model: {this.Model}, Processor: {this.Processor}, RAM: {this.RAM} GB, Hard Drive Size: {this.HardDriveSize} GB";
        }
    }

}
