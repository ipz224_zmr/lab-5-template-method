﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsClassLibrary
{
    public class FifthLevel : Handler
    {
        protected override string Report => "Очистіть оперативну пам'ять , \n  закрийте не потрібні вікна , \n перевірте на наявність вірусів";

        protected override int HelpLevel => 5;

        protected override string HandableProblem => "Пристрій повільно працює";

        public override void Inquiry(UserInquiry inquiry)
        {
            if (inquiry.UserProblem.Equals(this.HandableProblem, StringComparison.CurrentCultureIgnoreCase))
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine($"Рівень {this.HelpLevel} підтримка: {this.Report}");
                Console.ResetColor();
                return;
            }

            base.Inquiry(inquiry);
        }
    }
}
