﻿using AbstractFactoryLibrary;

class Program
{
    private static void ShowInfomationByManufacturer(IDeviceFactory deviceFactory)
    {
        Console.WriteLine($"{deviceFactory.Manufacturer} devices:");
        Console.WriteLine(deviceFactory.CreateEBook());
        Console.WriteLine(deviceFactory.CreateSmartphone());
        Console.WriteLine(deviceFactory.CreateLaptop());
        Console.WriteLine(deviceFactory.CreateNetbook());
        Console.WriteLine();
    }

    static void Main(string[] args)
    {

        IDeviceFactory iProneFactory = new IProneFactory();
        IDeviceFactory boomFactory = new BoomFactory();
        IDeviceFactory balaxyFactory = new BalaxyFactory();
        IDeviceFactory netFactory = new NetFactory();
        ShowInfomationByManufacturer(iProneFactory);
        ShowInfomationByManufacturer(boomFactory);
        ShowInfomationByManufacturer(balaxyFactory);
        ShowInfomationByManufacturer(netFactory);
        Console.ReadLine();
    }
}