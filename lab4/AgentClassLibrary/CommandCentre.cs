﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentClassLibrary
{
    public class CommandCentre
    {
        private List<Runway> _runways = new List<Runway>();
        private List<Aircraft> _aircrafts = new List<Aircraft>();
        public CommandCentre(List<Runway> runways, List<Aircraft> aircrafts)
        {
            this._runways.AddRange(runways);
            this._aircrafts.AddRange(aircrafts);
        }


        public void AircraftLandingInquiry(Aircraft aircraft)
        {
            Console.WriteLine($"Aircraft {aircraft.Name} inquiryes landing.");

            if (this._runways.Any(r => r.IsBusyWithAircraft == aircraft))
            {
                PrintText.PrintError($"Aircraft {aircraft.Name} already landed on Runway({aircraft.CurrentRunway!.Id})   {aircraft.CurrentRunway.Way}  \n");
                return;
            }

            foreach (var runway in this._runways)
            {
                if (runway.IsAvailable())
                {
                    PrintText.PrintSuccess($"Runway({runway.Id})  {runway.Way} is free. Aircraft {aircraft.Name} is landing.\n");
                    runway.IsBusyWithAircraft = aircraft;
                    aircraft.CurrentRunway = runway;
                    aircraft.Land();
                    return;
                }
            }

            PrintText.PrintError($"All runways are busy. Aircraft {aircraft.Name} cannot land.");
        }

        public void AircraftTakeOffInquiry(Aircraft aircraft)
        {
            Console.WriteLine($"Aircraft {aircraft.Name} inquiryes takeoff.");

            var runway = aircraft.CurrentRunway;
            if (runway != null && runway.IsBusyWithAircraft == aircraft)
            {
                PrintText.PrintSuccess($"Aircraft {aircraft.Name} is taking off from Runway({runway.Id})   {runway.Way}.");
                runway.IsBusyWithAircraft = null;
                aircraft.TakeOff();
            }
            else
            {
                PrintText.PrintError($"Aircraft {aircraft.Name} is not on any runway. Cannot takeoff.");
            }
            Console.WriteLine();
        }

      
    }
}
