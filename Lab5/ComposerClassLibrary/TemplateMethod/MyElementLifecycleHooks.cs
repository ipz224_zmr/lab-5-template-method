﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComposerClassLibrary.TemplateMethod
{
    public class MyElementLifecycleHooks : ElementLifecycleHooks
    {
        public override void OnCreated(LightElementNode element)
        {
            Console.WriteLine($"Element {element.TagName} has been created.");
        }

        public override void OnInserted(LightElementNode element)
        {
            Console.WriteLine($"Text has been inserted into element {element.TagName}.");
        }


        public override void OnClassListApplied(LightElementNode element)
        {
            Console.WriteLine($"Classes have been applied to element {element.TagName}.");
        }
    }
}
