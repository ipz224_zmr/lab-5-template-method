﻿namespace ClassLibrarySOLID
{
    public class Money
    {
        public int WholePart { get;  set; }
        public int Cents { get;  set; }

        public Money(int wholePart, int cents)
        {
            WholePart = wholePart;
            Cents = cents;
        }

        public static string DisplayAmount<T>(T item)
        {
            if (item is WarehouseItem warehouseItem && typeof(T) == typeof(WarehouseItem))
            {
                return $"{warehouseItem.Price.WholePart}.{warehouseItem.Price.Cents:00} \n";
            }
            else if (item is Money moneyItem && typeof(T) == typeof(Money))
            {
                return $"{moneyItem.WholePart}.{moneyItem.Cents:00}";
            }
            else
            {
                return $"";
            }
        }


        public void ApplyDiscount(Money discount)
        {
            int totalCents = (WholePart * 100 + Cents) - (discount.WholePart * 100 + discount.Cents);

            if (totalCents < 0)
            {
                totalCents = 0;
            }

            WholePart = totalCents / 100;
            Cents = totalCents % 100;
        }
    }

}