﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComposerClassLibrary.TemplateMethod
{
    public abstract class ElementLifecycleHooks
    {
        public virtual void OnCreated(LightElementNode element) { }
        public virtual void OnInserted(LightElementNode element) { }
  
        public virtual void OnClassListApplied(LightElementNode element) { }

        public void RunHooks(LightElementNode element)
        {
            OnCreated(element);
            if (element.ChildNodes.Exists(node => node.GetType() == typeof(LightTextNode)))
                OnInserted(element);
            if (element.CSSClasses.Count > 0)
                OnClassListApplied(element);
         
        }
    }
}
